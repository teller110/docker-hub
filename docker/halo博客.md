### Docker部署halo博客

```
docker run -it -d --name halo -p 8090:8090 -v ./halo2:/root/.halo2 halohub/halo:2
```



地址： `IP:8090`

后台路径：`/console`


---

***国内阿里云镜像***
```
registry.fit2cloud.com/halo/halo
```

---

### `docker-compose.yaml`部署

```
services:
  halo:
    image: halohub/halo:2
    restart: always
    depends_on:
      - mysql
    ports:
      - "8090:8090"
    volumes:
      - ./halo2:/root/.halo2
    command:
      - --spring.r2dbc.url=r2dbc:pool:mysql://mysql:3306/halo
      - --spring.r2dbc.username=halo
      - --spring.r2dbc.password=halo_password
      - --spring.sql.init.platform=mysql
      - --halo.external-url=http://公网IP:8090/

  mysql:
    image: mysql:5.7
    container_name: mysql
    environment:
      MYSQL_DATABASE: halo                  # 数据库名称
      MYSQL_USER: halo                      # 数据库用户名
      MYSQL_PASSWORD: halo_password         # 数据库密码
      MYSQL_ROOT_PASSWORD: halo_password    # 数据库root密码
    volumes:
      - ./data/mysql:/var/lib/mysql         # 映射数据库文件
    restart: always
```




---


---


## Docker部署typecho



```
services:
  mysql:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: typechoPASSWORD
      MYSQL_DATABASE: typecho
      MYSQL_USER: typecho
      MYSQL_PASSWORD: typechoPASSWORD
    volumes:
      - ./mysql-data:/var/lib/mysql

  typecho:
    image: joyqi/typecho:nightly-php8.2-apache
    restart: always
    depends_on:
      - mysql
    ports:
      - "8080:80"
    environment:
      TIMEZONE: Asia/Shanghai
      TYPECHO_SITE_URL: https://your-domain.com
      TYPECHO_DB_HOST: mysql
      TYPECHO_DB_NAME: typecho
      TYPECHO_DB_USER: typecho
      TYPECHO_DB_PASS: typechoPASSWORD
    volumes:
      - ./typecho-data:/var/www/html
```

`https://your-domain.com`替换为你的网址，提前反代好。

（可选）重置后台管理员密码：删除站点目录的`config.inc.php`文件，然后重新安装选择保留原有数据库即可

#### 主题文件目录`usr/themes`

安装git:
```
sudo apt install git
```

下载主题：
```
git clone 主题Git仓库地址
```
复制到主题目录：
```
docker cp 主题文件路径 typecho-server:/app/usr/themes
```

[仓库地址](https://hub.docker.com/r/joyqi/typecho)